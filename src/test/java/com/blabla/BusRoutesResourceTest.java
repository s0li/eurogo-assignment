package com.blabla;

import javax.ws.rs.RuntimeType;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Feature;

import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class BusRoutesResourceTest {

	private HttpServer server;
	private WebTarget  target;

	@Before
	public void setUp() throws Exception {
		// start the server
		server = Main.startServer(Utils.createInputStreamFrom(
				"3\n" +
				"0 0 1 2 3 4\n" +
				"1 3 1 6 5\n" +
				"2 0 6 4\n"
		));

		Client c = ClientBuilder.newClient();
		target = c.target(Main.BASE_URI);
	}

	@After
	public void tearDown() throws Exception {
		server.stop();
	}

	@Test
	public void testExistingRoute() {
		DirectBusRouteMsg responseMsg = target
				.path("direct")
				.queryParam("dep_sid" ,3)
				.queryParam("arr_sid", 6)
				.request()
				.get(DirectBusRouteMsg.class);

		assertEquals(3, responseMsg.dep_sid);
		assertEquals(6, responseMsg.arr_sid);
		assertEquals(true, responseMsg.direct_bus_route);
	}

	@Test
	public void testNonExistingRoute() {
		DirectBusRouteMsg responseMsg = target
				.path("direct")
				.queryParam("dep_sid" ,20)
				.queryParam("arr_sid", 6)
				.request()
				.get(DirectBusRouteMsg.class);

		assertEquals(20, responseMsg.dep_sid);
		assertEquals(6, responseMsg.arr_sid);
		assertEquals(false, responseMsg.direct_bus_route);
	}
}
