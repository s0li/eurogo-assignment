package com.blabla;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.blabla.Utils.createInputStreamFrom;

public class BusRoutesParserTest {


	@Test
	public void testInvalidNumBusRoutes() throws IOException,
																							 BusRoutesParser.MalformedBusRoutesException
	{
		try {
			BusRoutesParser.parse(createInputStreamFrom("jfewfa2\n"));
		}
		catch (NumberFormatException e) {
		}
	}

	@Test
	public void testCorrectParsing() throws IOException, BusRoutesParser.MalformedBusRoutesException {
		Map<Integer, List<Integer>> result = BusRoutesParser.parse(createInputStreamFrom(
				"3\n" +
				"0 0 1 2 3 4\n" +
				"1 3 1 6 5\n" +
				"2 0 6 4\n"
		));

		Assert.assertEquals(result.size(), 3);

		Assert.assertNotNull(result.get(0));
		Assert.assertTrue(result.get(0).equals(new ArrayList<Integer>(
				Arrays.asList(
						0, 1, 2, 3, 4
				))));

		Assert.assertNotNull(result.get(1));
		Assert.assertTrue(result.get(1).equals(new ArrayList<Integer>(
				Arrays.asList(
						3, 1, 6, 5
				))));

		Assert.assertNotNull(result.get(2));
		Assert.assertTrue(result.get(2).equals(new ArrayList<Integer>(
				Arrays.asList(
						0, 6, 4
				))));
	}
}
