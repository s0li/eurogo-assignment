package com.blabla;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BusRoutesParser {
	public static class MalformedBusRoutesException extends Exception {

	}

	public static Map<Integer, List<Integer>> parse(InputStream in) throws IOException,
																																				 MalformedBusRoutesException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		int numRoutes = Integer.valueOf(reader.readLine());

		Map<Integer, List<Integer>> result = new HashMap<>(numRoutes);

		for (int i = 0; i < numRoutes; ++i) {
			String routeLine = reader.readLine();
			String[] routeLineParts = routeLine.split(" ");

			if (routeLineParts.length == 0) {
				throw new MalformedBusRoutesException();
			}

			Integer lineId = Integer.valueOf(routeLineParts[0]);
			List<Integer> stopIds = new ArrayList<>();

			for (int j = 1; j < routeLineParts.length; ++j) {
				stopIds.add(Integer.valueOf(routeLineParts[j]));
			}

			result.put(lineId, stopIds);
		}

		return result;
	}
}
