package com.blabla;

import javax.ws.rs.*;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;


@Path("{routeType}")
public class BusRoutesResource {

	@Context
	private Configuration               conf_;
	private Map<Integer, List<Integer>> busRoutes_;


	/**
	 * Since conf_ is only injected when GET is actually called we do
	 * late initialization of busRoutes_.
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Map<Integer, List<Integer>> getBusRoutes() {
		if (busRoutes_ != null) {
			return busRoutes_;
		}

		if (conf_ == null) {
			throw new BusRoutesDataUnavailableException();
		}

		Map<Integer, List<Integer>> result = (Map<Integer, List<Integer>>)
				conf_.getProperty(Main.BusRoutesKey);

		if (result == null) {
			throw new BusRoutesDataUnavailableException();
		}

		busRoutes_ = result;
		return result;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public DirectBusRouteMsg getBusRoute(@PathParam("routeType") String routeType,
																			 @QueryParam("dep_sid") int departingStationId,
																			 @QueryParam("arr_sid") int arrivingStationId)
	{
		if (!routeType.equals("direct")) {
			throw new RouteTypeUnsupportedException(routeType);
		}

		for (Map.Entry<Integer, List<Integer>> route : getBusRoutes().entrySet()) {
			boolean foundDep = false;
			boolean foundArr = false;

			for (Integer stationId : route.getValue()) {
				if (stationId == departingStationId && !foundDep && !foundArr) {
					foundDep = true;
				}
				else if (stationId == arrivingStationId && foundDep && !foundArr) {
					foundArr = true;
				}

				if (foundDep && foundArr) {
					return new DirectBusRouteMsg(departingStationId, arrivingStationId, true);
				}
			}
		}

		return new DirectBusRouteMsg(departingStationId, arrivingStationId, false);
	}
}
