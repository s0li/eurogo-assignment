package com.blabla;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceException;


public class RouteTypeUnsupportedException extends WebApplicationException {
	public RouteTypeUnsupportedException() {
		super(Response.status(Response.Status.NOT_FOUND).build());
	}

	public RouteTypeUnsupportedException(String routeType) {
		super(Response.status(Response.Status.NOT_FOUND)
							.entity(String.format("'%s' routing type is not supported", routeType))
							.type("text/plain").build());
	}
}
