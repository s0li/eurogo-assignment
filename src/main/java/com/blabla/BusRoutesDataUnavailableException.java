package com.blabla;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class BusRoutesDataUnavailableException extends WebApplicationException {
	public BusRoutesDataUnavailableException() {
		super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
							.entity("Bus service data is unavailable")
							.type("text/plain")
							.build());
	}
}
