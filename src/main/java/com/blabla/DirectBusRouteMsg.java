package com.blabla;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Describes a direct route between two stations; whether or not it exists.
 */
@XmlRootElement
public class DirectBusRouteMsg {
	public int dep_sid;
	public int arr_sid;
	public boolean direct_bus_route;

	public int getDep_sid() {
		return dep_sid;
	}

	public void setDep_sid(int dep_sid) {
		this.dep_sid = dep_sid;
	}

	public int getArr_sid() {
		return arr_sid;
	}

	public void setArr_sid(int arr_sid) {
		this.arr_sid = arr_sid;
	}

	public boolean getDirect_bus_route() {
		return direct_bus_route;
	}

	public void setDirect_bus_route(boolean direct_bus_route) {
		this.direct_bus_route = direct_bus_route;
	}

	public DirectBusRouteMsg()
	{
		dep_sid = 0;
		arr_sid = 0;
		direct_bus_route = false;
	}

	public DirectBusRouteMsg(int dep_sid, int arr_sid, boolean direct_bus_route) {
		this.dep_sid = dep_sid;
		this.arr_sid = arr_sid;
		this.direct_bus_route = direct_bus_route;
	}
}
