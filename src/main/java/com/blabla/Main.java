package com.blabla;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class.
 */
public class Main {
	// Base URI the Grizzly HTTP server will listen on
	public static final String BASE_URI = "http://localhost:8088/api";

	public static final String BusRoutesKey = "BUS_ROUTES_KEY";

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
	 *
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer(InputStream confStream) throws IOException,
																																			BusRoutesParser.MalformedBusRoutesException
	{
		// create a resource config that scans for JAX-RS resources and providers
		// in com.blabla package
		final ResourceConfig rc = new ResourceConfig().packages("com.blabla");

		Map<String, Object> params = new HashMap<>();
		params.put("com.sun.jersey.config.property.packages", "com.blabla");
		params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
		params.put(BusRoutesKey, BusRoutesParser.parse(confStream));

		rc.addProperties(params);
		rc.register(JacksonFeature.class);

		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}

	private static void printUsage() {
		System.out.println("usage: start <routes file>");
	}

	/**
	 * Main method.
	 *
	 * @param args
	 *
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException,
																								BusRoutesParser.MalformedBusRoutesException
	{
		if (args.length != 1) {
			printUsage();
			return;
		}

		startServer(new FileInputStream(args[0]));
	}
}

